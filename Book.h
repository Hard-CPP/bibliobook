#ifndef _BOOK_H_
#define _BOOK_H_

#include "Author.h"

#include <string>

using namespace std;

class Book
{
    public:
        Book() {}
        ~Book();

        void setTitle(std::string _title) { title = _title; }
        void setAuthor(Author* _author) { author = author; }

        std::string getTitle() { return title; }
        Author* getAuthor() { return author; }

    private:
        std::string title;
        Author* author;
};

#endif