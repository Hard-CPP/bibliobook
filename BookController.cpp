#include "BookController.h"

using namespace std;

BookController* BookController::_INSTANCE = nullptr;

BookController* BookController::getInstance() {
	if (!_INSTANCE) {
		_INSTANCE = new BookController();
	}

	return _INSTANCE;
}

Book* BookController::GetBook(std::string title) const
{
	BookContainer::const_iterator itr = _bookStore.find(title);
	if (itr == _bookStore.end())
		return nullptr;

	return itr->second;
}

std::vector<Book*> BookController::GetBooks(Author* author) const
{
	std::vector<Book*> _bookList;

	for (auto itr = _bookStore.begin(); itr != _bookStore.end(); ++itr) {
		_bookList.push_back(itr->second);
	}

	return _bookList;
}

void BookController::RemoveBook(Book* book)
{
	BookContainer::const_iterator itr = _bookStore.find(book->getTitle());
	if (itr != _bookStore.end())
	{
		_bookStore.erase(itr);
		delete book;
	}
}

Author* BookController::GetAuthor(std::string name) const
{
	AuthorContainer::const_iterator itr = _authorStore.find(name);
	if (itr == _authorStore.end())
		return nullptr;

	return itr->second;
}

void BookController::RemoveAuthor(Author* author)
{
	for (auto book : GetBooks(author))
	{
		RemoveBook(book);
		delete book;
	}

	AuthorContainer::const_iterator itr = _authorStore.find(author->getName());
	if (itr != _authorStore.end())
	{
		_authorStore.erase(itr);
		delete author;
	}
}

int BookController::GetMenu()
{
	std::cout << "######### MENU ########" << endl;
	std::cout << "1 - Add Book" << endl;
	std::cout << "2 - Add Author" << endl;
	std::cout << "3 - Remove Book" << endl;
	std::cout << "4 - Remove Author" << endl;
	std::cout << "5 - Search Book by title" << endl;
	std::cout << "6 - Search Books by author" << endl;
	std::cout << "7 - Quit" << endl;

	int choice;
	std::cin >> choice;
	return choice;
}