#include "Author.h"
#include "Book.h"
#include "BookController.h"

using namespace std;

int main() {

	BookController* controller = BookController::getInstance();
menu:
	switch (controller->GetMenu())
	{
	case 1:
	{
		std::cout << "######### ADD BOOK ########" << endl;
		std::cout << "Enter book name :" << endl;
		std::string title;
		std::cin >> title;

		Book* book = controller->GetBook(title);
		if (book)
		{
			std::cout << "This book is already exist" << endl;
			delete book;
			system("pause");
		}

		book = new Book();
		book->setTitle(title);

		std::cout << "Enter author name :" << endl;
		std::string authorName;
		std::cin >> authorName;

		Author* author = controller->GetAuthor(authorName);
		if (!author)
		{
			std::cout << "This author doesn't exist" << endl;
			delete book;
			system("pause");
		}
		else {
			book->setAuthor(author);

			controller->AddBook(book);

			std::cout << "Book " + book->getTitle() + " has been added." << endl;
		}

		goto menu;
		break;
	}
	case 2:
	{
		std::cout << "######### ADD AUTHOR ########" << endl;
		std::cout << "Enter author name :" << endl;
		std::string authorName;
		std::cin >> authorName;

		Author* author = controller->GetAuthor(authorName);
		if (author)
		{
			std::cout << "This author is already existing" << endl;
			delete author;
			system("pause");
		}
		else {
			author = new Author();
			author->setName(authorName);

			controller->AddAuthor(author);

			std::cout << "Author " + author->getName() + " has been created." << endl;
		}

		goto menu;
		break;
	}
	case 3:
	{
		std::cout << "######### REMOVE BOOK ########" << endl;
		std::cout << "Enter book name :" << endl;
		std::string title;
		std::cin >> title;

		Book* book = controller->GetBook(title);
		if (!book)
		{
			std::cout << "This book doesn't exist" << endl;
			delete book;
			system("pause");
		}
		else {
			std::cout << "Book " + book->getTitle() + " has been removed." << endl;
			controller->RemoveBook(book);
		}

		goto menu;
		break;
	}
	case 4:
	{
		std::cout << "######### REMOVE AUTHOR ########" << endl;
		std::cout << "Enter author name :" << endl;
		std::string authorName;
		std::cin >> authorName;

		Author* author = controller->GetAuthor(authorName);
		if (!author)
		{
			std::cout << "This author doesn't exist" << endl;
			delete author;
			system("pause");
		}
		else {
			std::cout << "Author " + author->getName() + " and all his books has been removed." << endl;
			controller->RemoveAuthor(author);
		}

		goto menu;
		break;
	}
	case 5:
	{
		std::cout << "######### RESEARCH ########" << endl;
		std::cout << "Enter expected book title :" << endl;
		std::string title;
		std::cin >> title;

		Book* book = controller->GetBook(title);
		if (!book)
		{
			std::cout << "This book doesn't exist" << endl;
			delete book;
			system("pause");
		}
		else {
			std::cout << "Title : " + book->getTitle() + "\nAuthor : " + book->getAuthor()->getName() << endl;
		}

		goto menu;
		break;
	}
	case 6:
	{
		std::cout << "######### RESEARCH ########" << endl;
		std::cout << "Enter expected author name :" << endl;
		std::string authorName;
		std::cin >> authorName;

		Author* author = controller->GetAuthor(authorName);
		if (!author)
		{
			std::cout << "This author doesn't exist" << endl;
			delete author;
			system("pause");
		}
		else {
			std::cout << "Book list for author : " + author->getName() << endl;
			for (auto book : controller->GetBooks(author)) {
				std::cout << "  # " + book->getTitle() << endl;
			}
		}

		goto menu;
		break;
	}
	default:
		exit(0);
		break;
	}
}