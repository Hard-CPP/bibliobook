#ifndef _BOOKCONTROLLER_H_
#define _BOOKCONTROLLER_H_

#include "Author.h"
#include "Book.h"

#include <iostream>
#include <map>
#include <vector>

class BookController
{
    public:
        static BookController* getInstance();

        typedef std::map<std::string, Book*> BookContainer;
        typedef std::map<std::string, Author*> AuthorContainer;

        BookContainer const* GetBookStore() const { return &_bookStore; }
        AuthorContainer const* GetAuthorStore() const { return &_authorStore; }

        Book* GetBook(std::string /*title*/) const;
        std::vector<Book*> GetBooks(Author* author) const;
        void AddBook(Book* book) { _bookStore[book->getTitle()] = book; }
        void RemoveBook(Book* /*book*/);

        Author* GetAuthor(std::string /*name*/) const;
        void AddAuthor(Author* author) { _authorStore[author->getName()] = author; }
        void RemoveAuthor(Author* /*author*/);

        int GetMenu();
    private:
        BookController() {}
        BookController(BookController const&) {}

        static BookController* _INSTANCE;

        BookContainer _bookStore;
        AuthorContainer _authorStore;
};

#endif