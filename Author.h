#ifndef _AUTHOR_H_
#define _AUTHOR_H_

#include <string>

class Author
{
    public:
        Author();
        ~Author();

        void setName(std::string _name) { name = _name; }
        void setNickName(std::string _nickName) { nickname = _nickName; }

        std::string getName() { return name; }
        std::string getNickName() { return nickname; }

    private:
        std::string name;
        std::string nickname;
};

#endif